//异步读取前端配置
is_config();
//定义全局变量
let myTimer;
//JSON提交请求支付订单
if (document.getElementById("btn_submit")) {
    let loading = $('#loading');
    $("#btn_submit").click(function () {
        //加载模拟动画进度
        loading.on('show.bs.modal', function () {
            $(this).css('display', 'block');
            let modalHeight = $(window).height() / 2 - $('#loading .modal-dialog').height() / 2;
            $(this).find('.modal-dialog').css({
                'margin-top': modalHeight
            });
        });

        loading.modal('show');
        //异步请求提交订单数据
        let ordertype = $("input[name='channel']:checked").val();
        $.ajax({
            url: "/pay",
            type: "post",
            dataType: "json",
            data: {
                "ordername": $("#ordername").val(),
                "orderno": $("#orderno").val(),
                "ordermoney": $("#ordermoney").val(),
                "orderremark": $("#orderremark").val(),
                "ordertype": ordertype
            },
            success: function (data) {
                if (data.code === 200) {
                    return swal({
                        title: "亲!:友情提醒!!",
                        text: '您所选的【商品价格】暂无库存，请选择其它【金额】支付!',
                        timer: 15000,
                        icon: "info"
                    });
                }
                if (data.code === 101) {
                    return swal({
                        title: "亲!:友情提醒!!",
                        text: data.message,
                        timer: 15000,
                        icon: "warning"
                    });
                }
                //异步请求
                let order_channel = data.orderchannel;
                let qr_url = decodeURIComponent(data.orderurl);
                document.getElementById("orderid").value = data.orderid;
                DomHtml({
                    tag: 'h1',
                    id: 'orderid',
                    style: "color:#ff9818",
                    innerHTML: '单号:【' + data.orderid + '】'
                });
                DomHtml({
                    tag: 'h1',
                    id: 'orderuserid',
                    style: "color:#ff9818",
                    innerHTML: '订单充值名称:【' + data.ordername + '】'
                });
                DomHtml({
                    tag: 'h1',
                    id: 'orderprice',
                    style: "color:#ff9818",
                    innerHTML: '订单实付金额:【￥' + data.orderprice + '】'
                });
                //判断支付类型
                let type, hrefSrc;
                if (ordertype === "wechat") {
                    type = "微信";
                    hrefSrc = "weixin://";
                } else if (ordertype === "alipay") {
                    type = "支付宝";
                    hrefSrc = "alipays://";
                } else if (ordertype === "unionpay") {
                    type = "云闪付";
                    hrefSrc = "upwrp://";
                }
                let cb_msg = '为了您能实时到帐，请务必在扫码付款时,手动输入金额【' + data.orderprice + '】元,否则无法实时到帐!';
                let h1dom = '【1】长按二维码保存图片=>打开' + type + '<br/>【2】选择相册里二维码图片=>扫码付款<br/>';
                let href_style = "font-size:1.2em;font-weight:bold;width:90px;cursor:pointer;";
                let href_css = "button button-3d button-action button-circle button-jumbo";
                DomHtml({tag: 'h1', id: 'pay_tips', innerHTML: h1dom});
                if (ordertype === "wechat" && order_channel === "0") {
                    if (is_mobile()) {
                        DomHtml({
                            tag: 'a',
                            id: 'open_app',
                            style: href_style,
                            css: href_css,
                            href: hrefSrc,
                            innerHTML: '打开' + type + ''
                        });
                    }
                } else if (ordertype === "wechat" && order_channel === "1") {
                    if (is_mobile()) {
                        DomHtml({
                            tag: 'a',
                            id: 'pay_app',
                            style: href_style,
                            css: href_css,
                            href: hrefSrc,
                            innerHTML: "复制付款金额"
                        });
                    } else {
                        swal('友情提醒:', cb_msg, 'info');
                    }
                } else if (ordertype === "alipay" && order_channel === "0") {
                } else if (ordertype === "alipay" && order_channel === "1") {
                    if (is_mobile()) {
                        DomHtml({
                            tag: 'a',
                            id: 'pay_app',
                            style: href_style,
                            css: href_css,
                            href: "#",
                            innerHTML: "复制付款金额"
                        });
                    } else {
                        swal('友情提醒:', cb_msg, 'info');
                    }
                } else if (ordertype === "alipay" && order_channel === "2") {
                } else if (ordertype === "unionpay" && order_channel === "0") {
                } else if (ordertype === "unionpay" && order_channel === "1") {
                    if (is_mobile()) {
                        DomHtml({
                            tag: 'a',
                            id: 'pay_app',
                            style: href_style,
                            css: href_css,
                            href: "#",
                            innerHTML: "复制付款金额"
                        });
                    } else {
                        swal('友情提醒:', cb_msg, 'info');
                    }
                } else {
                    return console.log("没有找到请求支付的参数,请检查配置");
                }
                //生成二维码
                MyQrCode({
                    id: "#order_qr",
                    qrText: qr_url,
                    label: type + "/" + get_Date()
                });
                //复制转帐金额
                if (document.getElementById("pay_app")) {
                    let domNode = document.getElementById('pay_app');
                    domNode.addEventListener('click', event => copy('' + data.orderprice + '', event));
                }
                //隐藏提交按钮
                $("#hide").hide();
                //时间定时器
                timer("180");
            }
        });
        setTimeout("$('#loading').modal('hide')", 200);//如果延时
    });
}

//下面操作都是自行封装的函数不用管了
function DomHtml(opt) {
    opt = opt || {};
    let fragment = document.createDocumentFragment(),
        tag = opt.tag,
        href = opt.href,
        id = opt.id || "",
        type = opt.type || "",
        innerHTML = opt.innerHTML || "",
        async = opt.async || "async",
        target = opt.target || "",
        style = opt.style || "",
        css = opt.css || "",
        width = opt.width || "100%",
        height = opt.height || document.body.scrollHeight + "px";

    if (!tag) {
        console.log("要创建的标签不能为空");
        return tag;
    }
    let dom = document.createElement('' + tag + '');
    let TagId = document.getElementById('' + id + '');
    if (!TagId && tag !== "link" && tag !== "script") {
        if (id) dom.setAttribute('id', '' + id + '');
    }
    //标签创建
    if (tag === "script" && href !== "") {
        dom.setAttribute('type', 'text/javascript');
        dom.setAttribute('charset', 'utf-8');
        dom.setAttribute("async", '' + async + '');
        dom.setAttribute("defer", "false");
        dom.setAttribute('src', href + '?time=' + new Date().getTime());

    } else if (tag === "link" && href !== "") {
        dom.setAttribute('rel', 'stylesheet');
        dom.setAttribute('type', 'text/css');
        dom.setAttribute('href', href + '?time=' + new Date().getTime());

    } else if (tag === "a" && href !== "") {
        dom.setAttribute('href', '' + href + '');
        dom.setAttribute('target', '' + target + '');
        dom.setAttribute('class', '' + css + '');
        dom.style.cssText = style;
        dom.innerHTML = innerHTML;

    } else if (tag === "iframe" && href !== "") {
        dom.setAttribute("src", '' + href + '');
        dom.setAttribute('width', '' + width + '');
        dom.setAttribute('height', 'auto');
        dom.setAttribute('frameBorder', '0');
        dom.setAttribute('scrolling', 'no');
        dom.setAttribute('vspace', '0');
        dom.setAttribute('hspace', '0');
        dom.setAttribute('allowTransparency', 'true');
        dom.style.cssText = style;
        dom.style.height = height;

    } else if (tag !== "link" && tag !== "script" && tag !== "a" && tag !== "iframe") {
        if (type) dom.setAttribute('type', '' + type + '');
        dom.setAttribute('class', '' + css + '');
        dom.style.cssText = style;
        dom.innerHTML = innerHTML;
    }
    if (!TagId) {
        fragment.appendChild(dom);
        document.body.appendChild(fragment);
    } else {
        fragment.appendChild(dom);
        TagId.appendChild(fragment);
    }
    return dom;
}

//支付查寻时间倒计时
function timer(intDiff) {
    $("#pay_status").css('display', 'block');
    myTimer = window.setInterval(function () {
        let day = 0,
            hour = 0,
            minute = 0,
            second = 0;//时间默认值
        if (intDiff > 0) {
            day = Math.floor(intDiff / (60 * 60 * 24));
            hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
            minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
            second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
        }
        if (minute <= 9) minute = '0' + minute;
        if (second <= 9) second = '0' + second;
        $('#hour_show').html('<s id="h"></s>' + hour + '时');
        $('#minute_show').html('<s></s>' + minute + '分');
        $('#second_show').html('<s></s>' + second + '秒');
        if (hour <= 0 && minute <= 0 && second <= 0) {
            pay_timeout();
            clearInterval(myTimer);
        }
        intDiff--;
        return setTimeout(function () {
            isPay();
        }, 5000);
    }, 1000);
    // return setInterval(function () {
    //     isPay();
    // }, 6000);
    // return setInterval('isPay()',5000);
}


//异步查寻订单是否支付成功	
function isPay() {
    let orderid = $('#orderid').val();
    //console.log(orderid);
    $.ajax({
        type: "GET",
        data: {
            "orderid": orderid
        },
        url: "/pay/ispay",
        dataType: "json",
        success: function (data) {
            if (data.orderstatus === "1") {
                window.clearInterval(myTimer);
                return swal({
                    title: '支付成功=>订单号:' + data.orderid + '金额:' + data.orderprice + '',
                    icon: 'success',
                    text: '6秒后前往商家主页.',
                    timer: 6000,
                }).then(() => {
                    location.replace('/item/result'); //同步跳转商家主页自行修改
                })
            }
        }
    })
}

//上面提交支付结束，下面调用函数					  
function MyQrCode(opt) {
    opt = opt || {};        				//参数初始化
    let id = opt.id,
        qrText = opt.qrText,
        render = opt.render || "image",
        ecLevel = opt.ecLevel || "H",
        label = opt.label || "QPay",
        img = opt.img || null;

    if (id === "null" || id === "undefined" || id === "") {
        console.log("二维码ID不能为空");
        return;
    }
    if (qrText === "null" || qrText === "undefined" || qrText === "") {
        console.log("二维码内容不能为空");
        return;
    }

    let options = {
        render: render,
        minVersion: 4,
        //maxVersion: 10,
        ecLevel: ecLevel,//识别度
        left: 0,
        top: 0,
        size: 200,
        fill: '#000',//二维码颜色
        background: '#ffffff',//背景颜色
        text: qrText,//二维码内容
        radius: 0.5,
        quiet: 1,
        //中间logo start
        mode: 2,
        mSize: 0.08,
        mPosX: 0.5,
        mPosY: 0.5,
        //中间logo end
        label: label,
        fontname: 'QPay',
        fontcolor: '#ff9818',
        //image:$('#img-buffer')[0],//logo图片
        image: img
    };
    $(id).empty().qrcode(options);
}

//获取当前时间，格式YYYY-MM-DD
function get_Date() {
    let now = new Date(),
        year = now.getFullYear(),      //年
        month = now.getMonth() + 1,    //月
        day = now.getDate(),           //日
        hh = now.getHours(),            //时
        mm = now.getMinutes();          //分
    // var ss = now.getSeconds();          //秒
    let clock = year + "/";
    if (month < 10) clock += "0";
    clock += month + "/";
    if (day < 10) clock += "0";
    clock += day + " ";
    if (hh < 10) clock += "0";
    clock += hh + ":";
    if (mm < 10) clock += '0';
    //  clock += mm + ":";
    clock += mm;
    //  if (ss < 10) clock += '0';
    //	clock += ss;
    return (clock);
}

// 超时重置支付
function pay_timeout() {
    window.clearInterval(myTimer);
    $("#pay_status").css('display', 'none');
    $("#order_qr").empty("*");
    location.assign('');
}

function is_config() {
    let led = $('#led');//缓存
    $.ajax({
        type: "GET",
        url: "/file/get_payconfig",
        dataType: "json",
        success: function (data) {
            //console.log(data);
            if (data.led) {
                led.html(data.led);
                led.marquee({
                    duration: 20000,
                    gap: 50,
                    delayBeforeStart: 0,
                    direction: 'left',
                    duplicated: true,
                    pauseOnHover: true
                });
            }
            if (data.customer) {
                $("#customer").html('<a href=" ' + data.customer + '" class="btn btn-primary" target="_blank">联系在线客服</a>');
            }
            if (data.wxchannel === "false" && data.alichannel === "false" && data.unchannel === "false") {
                $("#wxchannel").hide();
                $("#alichannel").hide();
                $("#unchannel").hide();
                alert("非常抱歉，支付系统暂时维护，请稍后访问");
                return false;
            } else if (data.wxchannel === "false" && data.alichannel === "false") {
                $("#wxchannel").hide();
                $("#alichannel").hide();
                $("#unradio").attr("checked", true);
            } else if (data.wxchannel === "false" && data.unchannel === "false") {
                $("#wxchannel").hide();
                $("#unchannel").hide();
                $("#aliradio").attr("checked", true);
            } else if (data.alichannel === "false" && data.unchannel === "false") {
                $("#alichannel").hide();
                $("#unchannel").hide();
                $("#wxchannel").attr("checked", true);
            } else if (data.wxchannel === "false") {
                $("#wxchannel").hide();
                $("#aliradio").attr("checked", true);
            } else if (data.alichannel === "false") {
                $("#alichannel").hide();
                $("#wxchannel").attr("checked", true);
            } else if (data.unchannel === "false") {
                $("#unchannel").hide();
                $("#wxchannel").attr("checked", true);
            }
        }
    });
}

function is_mobile() {
    let regex_match = /(Android|webOS|iPhone|iPad|iPod|BlackBerry|huawei|htc|Windows Phone|IEMobile|MQQBrowser|MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE)/i;
    let result = regex_match.exec(navigator.userAgent);
    if (result) {
        return true;
    } else {
        return null;
    }
}

function copy(text, event) {
    const cb = new ClipboardJS('.null', {
        text: () => text
    });
    cb.on('success', function (e) {
        console.log(e);
        cb.off('error');
        cb.off('success');
        return swal({
            title: "亲，已复制成功!!",
            text: '为了您能实时到帐，请务必在扫码付款时,粘贴或手动输入金额【' + text + '】元金额,否则无法实时到帐!',
            timer: 30000,
            icon: "success"
        });
    });
    cb.on('error', function (e) {
        console.log(e);
        cb.off('error');
        cb.off('success');
        return swal({
            title: "亲，复制失败!!",
            text: '为了您能实时到帐，请务必在扫码付款时,手动输入金额【' + text + '】元金额,否则无法实时到帐!',
            timer: 30000,
            icon: "warning"
        });
    });
    cb.onClick(event);
    return cb;
}