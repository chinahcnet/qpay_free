<?php

//主机地址
$config['db']['dbHost'] = '127.0.0.1';

//数据库用户名
$config['db']['dbUser'] = 'root';

//数据库密码
$config['db']['dbPass'] = 'root';

//数据库名称
$config['db']['dbName'] = 'qpay';

//数据库前缀
$config['db']['dbPrefix'] = 'qpay_';

//数据库连接 PDO
$config['db']['dbType'] = 'pdo';

//数据库端口
$config['db']['dbPort'] = 3306;

//数据库编码
$config['db']['dbChar'] = 'utf8';


//默认控制器和操作名
$config['defaultController'] = 'Item';

$config['defaultAction'] = 'index';

return $config;
