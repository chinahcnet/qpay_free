﻿
#
# Structure for table "qpay_admin"
#

DROP TABLE IF EXISTS `qpay_admin`;
CREATE TABLE `qpay_admin` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `createdat` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `updatedat` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `city` varchar(255) DEFAULT NULL COMMENT '登陆IP',
  `userstatus` tinyint(3) DEFAULT '0' COMMENT '用户状态默认0 可用',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理员表';

#
# Data for table "qpay_admin"
#

/*!40000 ALTER TABLE `qpay_admin` DISABLE KEYS */;
INSERT INTO `qpay_admin` VALUES (2,'payadmin','98d8a25269f7590e018f3a513c645f01','1552994857','2019-05-06 21:18:20','121.231.47.120',0);
/*!40000 ALTER TABLE `qpay_admin` ENABLE KEYS */;

#
# Structure for table "qpay_order"
#

DROP TABLE IF EXISTS `qpay_order`;
CREATE TABLE `qpay_order` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ordertype` varchar(100) DEFAULT NULL COMMENT 'wechat 微信 alipay 支付宝 unionpay 云闪付',
  `orderchannel` tinyint(3) DEFAULT NULL,
  `ordername` varchar(255) DEFAULT NULL COMMENT '用户名或商品名称',
  `orderid` varchar(255) DEFAULT NULL COMMENT '订单号',
  `orderno` varchar(255) DEFAULT NULL COMMENT '外部平台订单号',
  `ordercreatetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '订单创建时间',
  `orderupdaetime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '订单更新时间',
  `ordermoney` decimal(10,2) DEFAULT NULL COMMENT '订单金额',
  `orderprice` decimal(10,2) DEFAULT NULL COMMENT '实付金额',
  `orderstatus` tinyint(3) DEFAULT '0' COMMENT '0 待支付 1 已支付 2 已过期 3 已关闭',
  `orderip` varchar(255) DEFAULT NULL COMMENT '支付地区',
  `orderaccount` varchar(255) DEFAULT NULL COMMENT '收款人帐号',
  `orderremark` varchar(255) DEFAULT NULL COMMENT '付款人联系方式',
  `orderurl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=429 DEFAULT CHARSET=utf8 COMMENT='订单表';

#
# Data for table "qpay_order"
#


#
# Structure for table "qpay_qr"
#

DROP TABLE IF EXISTS `qpay_qr`;
CREATE TABLE `qpay_qr` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `paytype` varchar(100) DEFAULT NULL COMMENT 'wechat 微信 alipay 支付宝 unionpay云闪付',
  `paymode` int(11) DEFAULT NULL COMMENT '0 固定金额 1 非固定金额 2 PID 3 云闪付',
  `paystatus` tinyint(3) DEFAULT '0' COMMENT '0 启用 1 停用',
  `paymoney` decimal(10,2) DEFAULT NULL COMMENT '预设收款金额',
  `payprice` decimal(10,2) DEFAULT NULL COMMENT '实际收款金额',
  `payurl` varchar(255) DEFAULT NULL COMMENT '收款二维码',
  `payaccount` varchar(255) DEFAULT NULL COMMENT '收款帐号备注',
  `payoperate` varchar(255) DEFAULT NULL COMMENT '收款操作',
  `createtime` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '二维码添加时间',
  `updatetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '二维码更新时间',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='二维码';

#
# Data for table "qpay_qr"
#

